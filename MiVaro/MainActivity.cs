using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

namespace MiVaro
{
	[Activity (Label = "MiVaro",Theme = "@style/Theme.MainWindow")]
	public class MainActivity : Activity
	{
		private ImageButton buttonQuincena;
		private ImageButton buttonAfore;
		private ImageButton buttonPropina;
		private ImageButton buttonPrestamo;
		private ImageButton buttonAutofin;
		private ImageButton buttonAmort;
		private ImageButton buttonAcercaDe;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);
			buttonQuincena = FindViewById<ImageButton>(Resource.Id.ButtonMiQuincena);
			buttonAfore = FindViewById<ImageButton>(Resource.Id.ButtonAfore);
			buttonPropina = FindViewById<ImageButton>(Resource.Id.ButtonPropinas);
			buttonPrestamo = FindViewById<ImageButton>(Resource.Id.ButtonPrestamos);
			buttonAutofin = FindViewById<ImageButton>(Resource.Id.ButtonAuto);
			buttonAmort = FindViewById<ImageButton>(Resource.Id.ButtonAmort);
			buttonAcercaDe = FindViewById<ImageButton> (Resource.Id.btnAcercade);

			buttonQuincena.Click += delegate {
				StartActivity(typeof(MiQuincenaActivity));
			};
			buttonAfore.Click += delegate {
				StartActivity(typeof(Afore));
			};
			buttonPropina.Click += delegate {
				StartActivity(typeof(Propina));
			};
			buttonPrestamo.Click += delegate {
				StartActivity(typeof(Prestamo));
			};
			buttonAutofin.Click += delegate {
				StartActivity(typeof(Autofin));
			};
			buttonAmort.Click += delegate {
				StartActivity(typeof(Amortizacion));
			};

			buttonAcercaDe.Click += delegate {
				StartActivity(typeof(AcercaDe));
			};
		}
	}
}
