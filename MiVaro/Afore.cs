using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Graphics;

namespace MiVaro
{
	[Activity (Label = "MiQuincenaActivity",Theme = "@style/Theme.MainWindow")]			
	public class Afore : Activity
	{
		private Android.App.AlertDialog.Builder builder;
		private Android.App.AlertDialog.Builder builderAlert;
		private Button buttonElige;
		private Button buttonCalcularAfore;
		private RadioButton radioAfore36;
		private RadioButton radioAfore45;
		private RadioButton radioAfore59;
		private RadioButton radioAfore60;
		private TextView txtaforeSeleccionada;
		private EditText txtSueldoMensual;
		private EditText txtTasaIncremento;
		private EditText txtPorcentajeDeposito;
		private EditText txtAniosTrabajo;
		private EditText txtAnioPago;
		private EditText txtMontoInicial;
		private int seleccion = 1;
		private float [] arrayAfore36 = new float[12]{0.1428F, 14.07F,13.39F,13.38F,13.25F,12.78F,12.70F,12.66F,9.93F,8.83F,8.68F,6.45F};
		private float [] arrayAfore45 = new float[12]{0.1285F, 14.07F,13.39F,13.38F,13.25F,12.78F,12.70F,12.66F,9.93F,8.83F,8.68F,6.45F};
		private float [] arrayAfore59 = new float[12]{0.1141F, 14.07F,13.39F,13.38F,13.25F,12.78F,12.70F,12.66F,9.93F,8.83F,8.68F,6.45F};
		private float [] arrayAfore60 = new float[12]{0.1012F, 14.07F,13.39F,13.38F,13.25F,12.78F,12.70F,12.66F,9.93F,8.83F,8.68F,6.45F};
		private string[] arrayTxtAfore =new string[4]{"Invercap, 14.28%","Invercap, 12.85%","PensionISSSTE, 11.41%","Invercap, 10.12%"};
		private float rendimientoAfore = 0.1428F;
		private float SueldoMensual;
		private float TasaIncremento;
		private float PorcentajeDeposito;
		private float Anios;
		private float AnioRetiro;
		private float MontoInicial;
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			// Create your application here
			SetContentView (Resource.Layout.Afore);
			buttonElige = FindViewById<Button> (Resource.Id.buttonEligeAfore);
			buttonCalcularAfore = FindViewById<Button> (Resource.Id.buttonCalcularAfore);
			radioAfore36 = FindViewById<RadioButton> (Resource.Id.radioAfore36);
			radioAfore45 = FindViewById<RadioButton> (Resource.Id.radioAfore45);
			radioAfore59 = FindViewById<RadioButton> (Resource.Id.radioAfore59);
			radioAfore60 = FindViewById<RadioButton> (Resource.Id.radioAfore60);
			txtaforeSeleccionada = FindViewById<TextView> (Resource.Id.txtAfore);
			txtSueldoMensual = FindViewById<EditText> (Resource.Id.txtSueldoMensual);
			txtTasaIncremento = FindViewById<EditText> (Resource.Id.txtTasaIncremento);
			txtPorcentajeDeposito = FindViewById<EditText> (Resource.Id.txtPorcentajeDeposito);
			txtAniosTrabajo = FindViewById<EditText> (Resource.Id.txtAniosTrabajo);
			txtAnioPago = FindViewById<EditText> (Resource.Id.txtAnioPago);
			txtMontoInicial = FindViewById<EditText> (Resource.Id.txtMontoInicial);
			txtaforeSeleccionada.Text = arrayTxtAfore [0];
			builderAlert = new AlertDialog.Builder (this);
			builderAlert.SetTitle (Resource.String.app_name);
			builder = new AlertDialog.Builder (this);
			builder.SetTitle (Resource.String.app_name);
			builder.SetPositiveButton(Resource.String.listAforeOk, OkClicked);
			builder.SetNegativeButton(Resource.String.listAforeNot, CancelClicked);
			builder.SetSingleChoiceItems (Resource.Array.afore36, 0, ListClicked);
			AlertDialog alert = builderAlert.Create();
			alert.SetTitle (Resource.String.app_name);
			radioAfore36.Click += delegate {
				builder.SetSingleChoiceItems (Resource.Array.afore36, 0, ListClicked);
				txtaforeSeleccionada.Text = arrayTxtAfore [0];
				rendimientoAfore = arrayAfore36[0];
				seleccion = 1;
			};
			radioAfore45.Click += delegate {
				builder.SetSingleChoiceItems (Resource.Array.afore45, 0, ListClicked);
				txtaforeSeleccionada.Text = arrayTxtAfore [1];
				rendimientoAfore = arrayAfore45[0];
				seleccion = 2;
			};
			radioAfore59.Click += delegate {
				builder.SetSingleChoiceItems (Resource.Array.afore59, 0, ListClicked);
				txtaforeSeleccionada.Text = arrayTxtAfore [2];
				rendimientoAfore = arrayAfore59[0];
				seleccion = 3;
			};
			radioAfore60.Click += delegate {
				builder.SetSingleChoiceItems (Resource.Array.afore60, 0, ListClicked);
				rendimientoAfore = arrayAfore60[0];
				txtaforeSeleccionada.Text = arrayTxtAfore [3];
				seleccion = 4;
			};

			buttonElige.Click += delegate {
				builder.Show();
			};

			buttonCalcularAfore.Click += delegate {
				if (validation() == false){
					alert.SetMessage("Te faltan algunos campos por llenar");
					alert.Show();
				}
				else{
					SueldoMensual = System.Convert.ToSingle(txtSueldoMensual.Text);
					TasaIncremento = System.Convert.ToSingle(txtTasaIncremento.Text);
					TasaIncremento = 1.0F +TasaIncremento / 100.0F;
					Anios = System.Convert.ToSingle(txtAniosTrabajo.Text);
					PorcentajeDeposito = System.Convert.ToSingle(txtPorcentajeDeposito.Text);
					PorcentajeDeposito = PorcentajeDeposito / 100.0F;
					MontoInicial = System.Convert.ToSingle(txtMontoInicial.Text);
					AnioRetiro = System.Convert.ToSingle(txtAnioPago.Text);
					calculoDeAfore(rendimientoAfore, SueldoMensual, TasaIncremento, Anios, PorcentajeDeposito, MontoInicial, AnioRetiro);
				}

			};
		}

		private void calculoDeAfore(float rendimiento, float sueldoMensual, float tasaIncremento, float anios, float porcentajeDeposito, float montoInicial, float anioRetiro){
			var sueldo = 0F;
			var sueldosTotales = 13F;
			var deposito = 0F;
			var monto = 0F;
			var comision = 0F;
			var interes = 0F;
			var flujoFinal = 0F;
			var pagoAnual = 0F;
			var pagoMensual = 0F;
			for (int i = 0; i < anios; i++) {
				sueldo = sueldoMensual * sueldosTotales * (float)Math.Pow (tasaIncremento,i);
				deposito = porcentajeDeposito * sueldo;	
				comision = 0.02F * deposito;
				monto = montoInicial + deposito - comision;
				interes = monto * rendimiento;
				flujoFinal = monto + interes;
				montoInicial = flujoFinal;
			}
			pagoAnual = flujoFinal * ((0.05F * ((float)Math.Pow(1.05F,anioRetiro))) / ((float)Math.Pow(1.05F,anioRetiro) - 1.0F));
			interes = (0.05F/12F);
			anioRetiro = anioRetiro * 12F;
			pagoMensual = flujoFinal * ((interes * ((float)Math.Pow(1 + interes,anioRetiro))) / ((float)Math.Pow(1 + interes,anioRetiro) - 1.0F));
			AlertDialog alertAfore = builderAlert.Create();
			alertAfore.SetTitle (Resource.String.app_name);
			alertAfore.SetMessage (string.Format("Años de trabajo: {0}\n" +
								"Años para el pago del fondo de retiro: {1}\n" +
								"Total de ahorros: {2}\n" +
								"Pago anual del fondo de retiro: {3}\n" +
				"Pago mensual del fondo de retiro: {4}",anios, anioRetiro/12F, flujoFinal, pagoAnual, pagoMensual )
			);
			alertAfore.Show();
		}


		private void ListClicked(object sender, DialogClickEventArgs args)
		{
			var items = Resources.GetStringArray(Resource.Array.afore36);
			var rendimientos = arrayAfore36;
			if (seleccion == 1) {
				items = Resources.GetStringArray(Resource.Array.afore36);
				rendimientos = arrayAfore36;
			}
			if (seleccion == 2) {
				items = Resources.GetStringArray(Resource.Array.afore45);
				rendimientos = arrayAfore45;
			}
			if (seleccion == 3) {
				items = Resources.GetStringArray(Resource.Array.afore59);
				rendimientos = arrayAfore59;
			}
			if (seleccion == 4) {
				items = Resources.GetStringArray(Resource.Array.afore60);
				rendimientos = arrayAfore60;
			}


			rendimientoAfore = rendimientos [args.Which];
			txtaforeSeleccionada.Text = string.Format ("{0}", items [args.Which]);
		}

		private void OkClicked(object sender, DialogClickEventArgs args){
			var items = Resources.GetStringArray(Resource.Array.afore36);
		}


		private void CancelClicked(object sender, DialogClickEventArgs args){
		}
		
		Boolean validation(){
			bool retorno = false;
			if (txtSueldoMensual.Text == "" || txtSueldoMensual.Text == "." || txtTasaIncremento.Text == "" || txtTasaIncremento.Text == "." || txtPorcentajeDeposito.Text == "" || txtPorcentajeDeposito.Text == "." || txtAniosTrabajo.Text == "" || txtAnioPago.Text == "") {
				retorno = false;
			} else {
				retorno = true;
			}
			return retorno;
		}
	}

}

