﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace MiVaro
{
	[Activity (Label = "AcercaDe",Theme = "@style/Theme.MainWindow")]			
	public class AcercaDe : Activity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.AcercaDe);

			// Get our button from the layout resource,
			// and attach an event to it
			Button button = FindViewById<Button> (Resource.Id.myButton);

			button.Click += (sender, e) => {

				var uri = Android.Net.Uri.Parse ("http://www.facebook.com/miVaroApp");
				var intent = new Intent (Intent.ActionView, uri);  
				StartActivity (intent);
			};
		}
	}
}

