using System;
using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Mono.Data.Sqlite;
using System.Data;
namespace MiVaro
{
	[Activity (Label = "MiQuincenaActivity",Theme = "@style/Theme.MainWindow")]			
	public class MiQuincenaActivity : Activity
	{
		private Button btnGuardar;
		private Button btnHistorial;
		private EditText txtIngreso;
		private EditText txtEntretenimiento;
		private EditText txtComida;
		private EditText txtEmergencias;
		private EditText txtTransporte;
		private CheckBox checkIngreso;
		private CheckBox checkEntretenimiento;
		private CheckBox checkComida;
		private CheckBox checkEmergencias;
		private CheckBox checkTransporte;
		private Android.App.AlertDialog.Builder builderAlert;
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.MiQuincena);
			try{
				Console.WriteLine ("Creacion de la base de datos");
				createDatabase();
				Console.WriteLine ("Base de datos creada");
			}
			catch{
				builderAlert = new AlertDialog.Builder(this);
				AlertDialog alert = builderAlert.Create();
				alert.SetTitle (Resource.String.app_name);
				alert.SetMessage ("Tu telefono no soporta bases de datos.");
			}
			// Create your application here
			btnGuardar = FindViewById<Button> (Resource.Id.btnGuardar);
			btnHistorial = FindViewById<Button> (Resource.Id.btnHistorial);
			txtIngreso = FindViewById<EditText> (Resource.Id.txtIngreso);
			txtEntretenimiento = FindViewById<EditText> (Resource.Id.txtEntretenimiento);
			txtComida = FindViewById<EditText> (Resource.Id.txtComida);
			txtEmergencias = FindViewById<EditText> (Resource.Id.txtEmergencias);
			txtTransporte = FindViewById<EditText> (Resource.Id.txtTransporte);
			checkIngreso = FindViewById<CheckBox> (Resource.Id.checkIngreso);
			checkEntretenimiento = FindViewById<CheckBox> (Resource.Id.checkEntretenimiento);
			checkComida = FindViewById<CheckBox> (Resource.Id.checkComida);
			checkEmergencias = FindViewById<CheckBox> (Resource.Id.checkEmergencias);
			checkTransporte = FindViewById<CheckBox> (Resource.Id.checkTransporte);
	
			btnGuardar.Click += delegate {
				saveDataBase();
			};

			btnHistorial.Click += delegate {
				showHistorial();
			};
		}
		public void showHistorial(){

			builderAlert = new AlertDialog.Builder(this);
			AlertDialog alert = builderAlert.Create();
			alert.SetTitle (Resource.String.app_name);
			var gasto = new GastosUsuario();
			gasto.ID = 1;
			var gastoUsuarioIngreso = gasto.getGasto();
			gasto.ID = 2;
			var gastoUsuarioEntretenimiento = gasto.getGasto();
			gasto.ID = 3;
			var gastoUsuarioComida = gasto.getGasto();
			gasto.ID = 4;
			var gastoUsuarioEmergencias = gasto.getGasto();
			gasto.ID = 5;
			var gastoUsuarioTransporte = gasto.getGasto();
			alert.SetMessage (string.Format("Ingresos totales: {0}\n" +
				"Gastos totales por\n" +
				"Entretenimiento: {1}\n" +
				"Comida: {2}\n" +
				"Emergencias: {3}\n" +
				"Transporte: {4}", gastoUsuarioIngreso, gastoUsuarioEntretenimiento, gastoUsuarioComida, gastoUsuarioEmergencias, gastoUsuarioTransporte));
			alert.Show();
		}
		public void saveDataBase(){
			var newGasto = new HistorialGastos ();	
			var gasto = new GastosUsuario();
		if (checkIngreso.Checked == true && txtIngreso.Text != "" && txtIngreso.Text != ".") {
				gasto.ID = 1;
				var cnt = gasto.getGasto ();
				gasto.cantidadTotal = System.Convert.ToSingle(cnt) + System.Convert.ToSingle (txtIngreso.Text);
				gasto.updateGasto ();
				newGasto.tipoGasto = 1;
				newGasto.cantidad = System.Convert.ToSingle (txtIngreso.Text);
				newGasto.guardarHistorialGasto ();
			} else {
				builderAlert = new AlertDialog.Builder(this);
				AlertDialog alert = builderAlert.Create();
				alert.SetTitle (Resource.String.app_name);
				alert.SetMessage ("El campo esta vacio o incorrecto");
			}
		if (checkEntretenimiento.Checked == true && txtEntretenimiento.Text != "" && txtEntretenimiento.Text != ".") {
				gasto.ID = 2;
				var cnt = gasto.getGasto ();
				gasto.cantidadTotal = System.Convert.ToSingle(cnt) + System.Convert.ToSingle (txtEntretenimiento.Text);
				gasto.updateGasto ();
				newGasto.tipoGasto = 2;
				newGasto.cantidad = System.Convert.ToSingle (txtEntretenimiento.Text);
				newGasto.guardarHistorialGasto ();
			}
		if (checkComida.Checked == true && txtComida.Text != "" && txtComida.Text != ".") {
				gasto.ID = 3;
				var cnt = gasto.getGasto ();
				gasto.cantidadTotal = System.Convert.ToSingle(cnt) + System.Convert.ToSingle (txtComida.Text);
				gasto.updateGasto ();
				newGasto.tipoGasto = 3;
				newGasto.cantidad = System.Convert.ToSingle (txtComida.Text);
				newGasto.guardarHistorialGasto ();
			}
		if (checkEmergencias.Checked == true && txtEmergencias.Text != "" && txtEmergencias.Text != ".") {			
				gasto.ID = 4;
				var cnt = gasto.getGasto ();
				gasto.cantidadTotal = System.Convert.ToSingle(cnt) + System.Convert.ToSingle (txtEmergencias.Text);
				gasto.updateGasto ();
				newGasto.tipoGasto = 4;
				newGasto.cantidad = System.Convert.ToSingle (txtEmergencias.Text);
				newGasto.guardarHistorialGasto ();
			}
		if (checkTransporte.Checked == true && txtTransporte.Text != "" && txtTransporte.Text != ".") {
				gasto.ID = 5;
				var cnt = gasto.getGasto ();
				gasto.cantidadTotal = System.Convert.ToSingle(cnt) + System.Convert.ToSingle (txtTransporte.Text);
				gasto.updateGasto ();
				newGasto.tipoGasto = 5;
				newGasto.cantidad = System.Convert.ToSingle (txtTransporte.Text);
				newGasto.guardarHistorialGasto ();
			}
		}

	public Boolean validation(string txt){
			bool retorno = false;
			if (txt == "" || txt == ".") {
				retorno = false;
			} else {
				retorno = true;
			}
			return retorno;
	}

		public void createDatabase(){
			string dbPath = System.IO.Path.Combine (System.Environment.GetFolderPath (System.Environment.SpecialFolder.Personal), "mivaro.db3");
			bool exists = System.IO.File.Exists (dbPath);
			if (!exists) {
				Mono.Data.Sqlite.SqliteConnection.CreateFile (dbPath);
				var connection = new SqliteConnection ("Data Source=" + dbPath);
				var commands = new[] {
					"CREATE TABLE HistorialGastos (id INTEGER PRIMARY KEY  AUTOINCREMENT  NOT NULL , tipoGasto INTEGER NOT NULL , fecha DATETIME, cantidad FLOAT NOT NULL )",
					"CREATE TABLE GastosUsuario (id INTEGER PRIMARY KEY  NOT NULL , cantidadTotal FLOAT)",
				};
				connection.Open ();
				foreach (var command in commands) {
					using (var c = connection.CreateCommand ()) {
						c.CommandText = command;
						var rowcount = c.ExecuteNonQuery ();
					}
				}
				var gasto = new GastosUsuario ();
				for (int i = 1; i < 6; i++) {
					gasto.ID = i;
					gasto.cantidadTotal = 0.0F;
					gasto.guardarGasto ();
				}
			} else {
			}
		}

		public class HistorialGastos{
			public int ID{ get; set;}
			public int tipoGasto {get; set;}
			public DateTime fecha {get; set;}
			public float cantidad{ get; set;}
			private string dbPath = System.IO.Path.Combine (System.Environment.GetFolderPath (System.Environment.SpecialFolder.Personal), "mivaro.db3");

			public void guardarHistorialGasto(){
				var connection = new SqliteConnection ("Data Source=" + dbPath);
				connection.Open();
				var command = connection.CreateCommand();
				command.CommandText = "INSERT INTO HistorialGastos (tipoGasto, fecha, cantidad) VALUES (@tipoGasto,@fecha,@cantidad)";
				command.Parameters.AddWithValue("@tipoGasto", tipoGasto);
				command.Parameters.AddWithValue ("@fecha", DateTime.Now.ToString("M/d/yyyy"));
				command.Parameters.AddWithValue("@cantidad", cantidad);
				command.ExecuteNonQuery();
				connection.Close ();
			}
		}

		public class GastosUsuario{
			public int ID{ get; set;}
			public float cantidadTotal{ get; set;}
			private string dbPath = System.IO.Path.Combine (System.Environment.GetFolderPath (System.Environment.SpecialFolder.Personal), "mivaro.db3");

			public void guardarGasto(){
				var connection = new SqliteConnection ("Data Source=" + dbPath);
				connection.Open();
				var command = connection.CreateCommand();
				command.CommandText = "INSERT INTO GastosUsuario VALUES(@id,@cantidadTotal)";
				command.Parameters.AddWithValue("@id", ID);
				command.Parameters.AddWithValue("@cantidadTotal", cantidadTotal);
				command.ExecuteNonQuery();
				connection.Close ();
			}
			public string getGasto(){
				var connection = new SqliteConnection ("Data Source=" + dbPath);
				connection.Open();
				var command = connection.CreateCommand();
				command.CommandText = "SELECT * FROM GastosUsuario WHERE id = @id";
				command.Parameters.AddWithValue("@id", ID);
				var r = command.ExecuteReader();
				var retorno = r["cantidadTotal"].ToString();
				connection.Close ();
				return retorno;
			}

			public void updateGasto(){
				var connection = new SqliteConnection ("Data Source=" + dbPath);
				connection.Open();
				var command = connection.CreateCommand();
				command.CommandText = "UPDATE GastosUsuario SET cantidadTotal=@cantidadTotal WHERE id = @id";
				command.Parameters.AddWithValue("@id", ID);
				command.Parameters.AddWithValue("@cantidadTotal", cantidadTotal);
				var r = command.ExecuteNonQuery();
				connection.Close ();
			}
		
		}
	}
}