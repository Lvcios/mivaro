using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace MiVaro
{
	[Activity (Label = "Prestamo", Theme = "@style/Theme.MainWindow")]			
	public class Prestamo : Activity
	{
		private EditText capitalInicial;
		private EditText tasaInteres;
		private EditText meses;
		private float capInicia;
		private float tasa;
		private float mes;
		private Button buttonCalcular;
		private Android.App.AlertDialog.Builder builder;
		private string msj;
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Create your application here
			SetContentView (Resource.Layout.Prestamos);
			capitalInicial = FindViewById<EditText> (Resource.Id.capitalInicial);
			tasaInteres = FindViewById<EditText> (Resource.Id.tasa);
			meses = FindViewById<EditText> (Resource.Id.meses);
			buttonCalcular = FindViewById<Button> (Resource.Id.buttonCalcular);
			builder = new AlertDialog.Builder(this);
			AlertDialog alert = builder.Create();
			alert.SetTitle (Resource.String.app_name);
			buttonCalcular.Click += delegate {
				if (!validation()){
					msj = "Te faltan campos por llenar";
				}
				else{
					tasa = System.Convert.ToSingle(tasaInteres.Text);
					tasa = (tasa/12.0F) / 100.0F;
					capInicia = System.Convert.ToSingle(capitalInicial.Text);
					mes = System.Convert.ToSingle(meses.Text);
					msj = string.Format("Interes total por pagar: {0}\nCapital Inicial: {1}\nTotal a pagar: {2}", (tasa*capInicia*mes), capInicia,(capInicia +(tasa*capInicia*mes) ) );
					alert.SetMessage (msj);
					alert.Show();
				}
			};
		}

		Boolean validation(){
			bool valido = false;
			if (capitalInicial.Text == "" || tasaInteres.Text == "" || meses.Text == "" || capitalInicial.Text == "." || tasaInteres.Text == "." || meses.Text == ".") {
				valido = false;
			} else {
				valido = true;
			}
			return valido;
		}
	}
}

