using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace MiVaro
{
	[Activity (Label = "Amortizacion", Theme = "@style/Theme.MainWindow")]			
	public class Amortizacion : Activity
	{
		private Android.App.AlertDialog.Builder builder;
		private Android.App.AlertDialog.Builder builderAlert;
		private Button buttonCalculaAmort;
		private EditText txtMontoDeuda;
		private EditText txtPlazoDeuda;
		private EditText txtInteresFijo;
		private EditText txtPagoFinal;
		private float MontoDeuda;
		private float PlazoDeuda;
		private float InteresFijo;
		private float PagoFinal;
		private RadioButton radioCreciente;
		private RadioButton radioDecreciente;
		private RadioButton radioFlat;
		private RadioButton radioConstante;
		private int seleccion = 1;
		//private TableLayout template;
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Create your application here
			SetContentView (Resource.Layout.Amortizacion);
			txtMontoDeuda = FindViewById<EditText> (Resource.Id.txtMontoDeuda);
			txtPlazoDeuda = FindViewById<EditText> (Resource.Id.txtPlazoDeuda);
			txtInteresFijo = FindViewById<EditText> (Resource.Id.txtInteresFijo);
			txtPagoFinal = FindViewById<EditText> (Resource.Id.txtPagoFinal);
			radioCreciente = FindViewById<RadioButton> (Resource.Id.radioCreciente);
			radioDecreciente = FindViewById<RadioButton> (Resource.Id.radioDecreciente);
			radioFlat = FindViewById<RadioButton> (Resource.Id.radioFlat);
			radioConstante = FindViewById<RadioButton> (Resource.Id.radioConstante);
			buttonCalculaAmort = FindViewById<Button> (Resource.Id.buttonCalculaAmort);

			radioConstante.Click += delegate {
				seleccion = 1;
			};
			radioDecreciente.Click += delegate {
				seleccion = 2;
			};
			radioCreciente.Click += delegate {
				seleccion = 3;
			};
			radioFlat.Click += delegate {
				seleccion = 4;
			};
			builderAlert = new AlertDialog.Builder(this);
			AlertDialog alert = builderAlert.Create();
			alert.SetTitle (Resource.String.app_name);
			buttonCalculaAmort.Click += delegate {
				if (!validation()) {
					alert.SetMessage("Te faltan algunos campos por llenar o no lo hiciste correctamente.");
				}
				else{

					switch (seleccion)
					{
					case 1:
						cuotasConstantes();
						break;
					case 2:
						cuotasDecrecientes();
						break;
					case 3:
						cuotasCrecientes();
						break;
					case 4:
						cuotasDirectas();
						break;
					default:
						break;
					}
				}
				//cuotasDecrecientes();
				//cuotasConstantes();
				//cuotasCrecientes();
				//cuotasDirectas();
			};
		}

		public void cuotasDecrecientes() {
			ScrollView sv = new ScrollView(this);
			TableLayout ll=new TableLayout(this);
			HorizontalScrollView hsv = new HorizontalScrollView(this);
			TableRow tbrowHeader = new TableRow(this);
			TextView labelPeriodo = new TextView (this);
			TextView labelDeuda = new TextView (this);
			TextView labelAmortizacion = new TextView (this);
			TextView labelComision = new TextView (this);
			TextView labelSaldo = new TextView (this);
			TextView labelTotal = new TextView (this);
			labelPeriodo.Text = "Periodo - ";
			labelDeuda.Text = "Deuda remanente - ";
			labelAmortizacion.Text = "Amortizacion - ";
			labelComision.Text = "Comision - ";
			labelSaldo.Text = "Saldo - ";
			labelTotal.Text = "Total a pagar";
			tbrowHeader.AddView (labelPeriodo);
			tbrowHeader.AddView (labelDeuda);
			tbrowHeader.AddView (labelAmortizacion);
			tbrowHeader.AddView (labelComision);
			tbrowHeader.AddView (labelSaldo);
			tbrowHeader.AddView (labelTotal);
			ll.AddView(tbrowHeader);

			float sumaMontoRemanente=0F,sumaComision=0F,sumaSaldo=0F,sumaTotal=0F;
			MontoDeuda = System.Convert.ToSingle (txtMontoDeuda.Text);
			PagoFinal = System.Convert.ToSingle (txtPagoFinal.Text);
			PlazoDeuda = System.Convert.ToInt16 (txtPlazoDeuda.Text);
			InteresFijo = System.Convert.ToSingle (txtInteresFijo.Text) / 100F ;
			var amortizacion = (MontoDeuda - PagoFinal) / PlazoDeuda;
			var comision = 0F;
			var totalAPagar = 0F;
			TextView tvPeriodo = new TextView(this);
			TextView tvMontoAmort = new TextView(this); 	
			TextView tvAmortizacion = new TextView (this);
			TextView tvComision = new TextView (this);
			TextView tvMontoDeuda = new TextView (this);
			TextView tvTotalAPagar = new TextView (this);
			int i = 0;
			for(i=1;i<PlazoDeuda;i++) {
				TableRow tbrow = new TableRow(this);
				comision = MontoDeuda * InteresFijo;
				MontoDeuda = MontoDeuda - amortizacion;
				totalAPagar = amortizacion + comision;
				sumaMontoRemanente = sumaMontoRemanente + MontoDeuda + amortizacion;
				sumaComision = sumaComision + comision;
				sumaSaldo = sumaSaldo + MontoDeuda;
				sumaTotal = sumaTotal + totalAPagar;
				tvPeriodo.Text = System.Convert.ToString(i); 
				tvMontoAmort.Text = System.Convert.ToString(Math.Round(MontoDeuda + amortizacion,2)); 
				tvAmortizacion.Text = System.Convert.ToString(Math.Round(amortizacion,2)); 
				tvComision.Text = System.Convert.ToString(Math.Round(comision,2)); 
				tvMontoDeuda.Text = System.Convert.ToString(Math.Round(MontoDeuda,2)); 
				tvTotalAPagar.Text = System.Convert.ToString(Math.Round(totalAPagar,2));  
				tbrow.AddView(tvPeriodo);
				tbrow.AddView(tvMontoAmort);
				tbrow.AddView(tvAmortizacion);
				tbrow.AddView(tvComision);
				tbrow.AddView(tvMontoDeuda);
				tbrow.AddView(tvTotalAPagar);
				ll.AddView(tbrow);
			}

			TableRow tbrowPenultima = new TableRow(this);
			TextView tvPeriodoP = new TextView(this);
			TextView tvMontoAmortP = new TextView(this);
			TextView tvAmortizacionP = new TextView(this);
			TextView tvComisionP = new TextView(this);
			TextView tvMontoDeudaP = new TextView(this);
			TextView tvTotalAPagarP = new TextView(this);
			comision = MontoDeuda * InteresFijo;
			MontoDeuda = MontoDeuda - amortizacion;
			totalAPagar = amortizacion + comision + PagoFinal ;
			sumaMontoRemanente = sumaMontoRemanente + MontoDeuda + amortizacion;
			sumaComision = sumaComision + comision;
			sumaSaldo = sumaSaldo + MontoDeuda;
			sumaTotal = sumaTotal + totalAPagar;
			tvPeriodoP.Text = System.Convert.ToString(i); 
			tvMontoAmortP.Text = System.Convert.ToString(Math.Round((MontoDeuda + amortizacion),2)); 
			tvAmortizacionP.Text = System.Convert.ToString(Math.Round(amortizacion,2)); 
			tvComisionP.Text = System.Convert.ToString(Math.Round(comision,2)); 
			tvMontoDeudaP.Text = System.Convert.ToString(Math.Round(MontoDeuda,2)); 
			tvTotalAPagarP.Text = System.Convert.ToString(Math.Round(totalAPagar,2)); 
			tbrowPenultima.AddView(tvPeriodoP);
			tbrowPenultima.AddView(tvMontoAmortP);
			tbrowPenultima.AddView(tvAmortizacionP);
			tbrowPenultima.AddView(tvComisionP);
			tbrowPenultima.AddView(tvMontoDeudaP);
			tbrowPenultima.AddView(tvTotalAPagarP);
			ll.AddView(tbrowPenultima);


			TableRow tbrowUltima = new TableRow(this);
			TextView tvPeriodoF = new TextView(this);
			TextView tvMontoAmortF = new TextView(this);
			TextView tvAmortizacionF = new TextView(this);
			TextView tvComisionF = new TextView(this);
			TextView tvMontoDeudaF = new TextView(this);
			TextView tvTotalAPagarF = new TextView(this);
			tvPeriodoF.Text = "Totales";
			tvMontoAmortF.Text = System.Convert.ToString(Math.Round(sumaMontoRemanente,2)); 
			tvAmortizacionF.Text = System.Convert.ToString(Math.Round(amortizacion,2)); 
			tvComisionF.Text = System.Convert.ToString(Math.Round(sumaComision,2)); 
			tvMontoDeudaF.Text = System.Convert.ToString(Math.Round(sumaSaldo,2)); 
			tvTotalAPagarF.Text = System.Convert.ToString(Math.Round(sumaTotal,2));
			tbrowUltima.AddView(tvPeriodoF);
			tbrowUltima.AddView(tvMontoAmortF);
			tbrowUltima.AddView(tvAmortizacionF);
			tbrowUltima.AddView(tvComisionF);
			tbrowUltima.AddView(tvMontoDeudaF);
			tbrowUltima.AddView(tvTotalAPagarF);
			ll.AddView(tbrowUltima);

			hsv.AddView (ll);
			sv.AddView(hsv);
			builder = new AlertDialog.Builder (this);
			builder.SetTitle (Resource.String.app_name);
			builder.SetView(sv);
			builder.Create ();
			builder.Show ();
			}

		public void cuotasConstantes() {
			ScrollView sv = new ScrollView(this);
			TableLayout ll=new TableLayout(this);
			HorizontalScrollView hsv = new HorizontalScrollView(this);
			TableRow tbrowHeader = new TableRow(this);
			TextView labelPeriodo = new TextView (this);
			TextView labelDeuda = new TextView (this);
			TextView labelAmortizacion = new TextView (this);
			TextView labelComision = new TextView (this);
			TextView labelSaldo = new TextView (this);
			TextView labelTotal = new TextView (this);
			labelPeriodo.Text = "Periodo - ";
			labelDeuda.Text = "Deuda remanente - ";
			labelAmortizacion.Text = "Amortizacion - ";
			labelComision.Text = "Comision - ";
			labelSaldo.Text = "Saldo - ";
			labelTotal.Text = "Total a pagar";
			tbrowHeader.AddView (labelPeriodo);
			tbrowHeader.AddView (labelDeuda);
			tbrowHeader.AddView (labelAmortizacion);
			tbrowHeader.AddView (labelComision);
			tbrowHeader.AddView (labelSaldo);
			tbrowHeader.AddView (labelTotal);
			ll.AddView(tbrowHeader);

			float sumaMontoRemanente=0F,sumaComision=0F,sumaSaldo=0F,sumaTotal=0F,sumaAmortizacion=0F;
			MontoDeuda = System.Convert.ToSingle (txtMontoDeuda.Text);
			PagoFinal = System.Convert.ToSingle (txtPagoFinal.Text);
			PlazoDeuda = System.Convert.ToInt16 (txtPlazoDeuda.Text);
			InteresFijo = System.Convert.ToSingle (txtInteresFijo.Text) / 100F ;
			var totalAPagar = MontoDeuda * ((InteresFijo * ((float)Math.Pow(1F + InteresFijo,PlazoDeuda))) / ((float)Math.Pow(1F +  InteresFijo,PlazoDeuda) - 1.0F));	 
			var amortizacion = 0F;
			var comision = 0F;
			TextView tvPeriodo = new TextView(this);
			TextView tvMontoAmort = new TextView(this); 	
			TextView tvAmortizacion = new TextView (this);
			TextView tvComision = new TextView (this);
			TextView tvMontoDeuda = new TextView (this);
			TextView tvTotalAPagar = new TextView (this);
			int i = 0;
			for(i=1;i<PlazoDeuda;i++) {
				TableRow tbrow = new TableRow(this);
				comision = MontoDeuda * InteresFijo;
				amortizacion = totalAPagar - comision;
				MontoDeuda = MontoDeuda - amortizacion;
				sumaMontoRemanente = sumaMontoRemanente + MontoDeuda + amortizacion;
				sumaAmortizacion = sumaAmortizacion + amortizacion;
				sumaComision = sumaComision + comision;
				sumaSaldo = sumaSaldo + MontoDeuda;
				sumaTotal = sumaTotal + totalAPagar;
				tvPeriodo.Text = System.Convert.ToString(i); 
				tvMontoAmort.Text = System.Convert.ToString(Math.Round(MontoDeuda + amortizacion,2)); 
				tvAmortizacion.Text = System.Convert.ToString(Math.Round(amortizacion,2)); 
				tvComision.Text = System.Convert.ToString(Math.Round(comision,2)); 
				tvMontoDeuda.Text = System.Convert.ToString(Math.Round(MontoDeuda,2)); 
				tvTotalAPagar.Text = System.Convert.ToString(Math.Round(totalAPagar,2)); 
				tbrow.AddView(tvPeriodo);
				tbrow.AddView(tvMontoAmort);
				tbrow.AddView(tvAmortizacion);
				tbrow.AddView(tvComision);
				tbrow.AddView(tvMontoDeuda);
				tbrow.AddView(tvTotalAPagar);
				ll.AddView(tbrow);
			}

			TableRow tbrowPenultima = new TableRow(this);
			TextView tvPeriodoP = new TextView(this);
			TextView tvMontoAmortP = new TextView(this);
			TextView tvAmortizacionP = new TextView(this);
			TextView tvComisionP = new TextView(this);
			TextView tvMontoDeudaP = new TextView(this);
			TextView tvTotalAPagarP = new TextView(this);
			comision = MontoDeuda * InteresFijo;
			amortizacion = totalAPagar - comision;
			MontoDeuda = MontoDeuda - amortizacion;
			sumaMontoRemanente = sumaMontoRemanente + MontoDeuda + amortizacion;
			sumaAmortizacion = sumaAmortizacion + amortizacion;
			sumaComision = sumaComision + comision;
			sumaSaldo = sumaSaldo + MontoDeuda;
			tvPeriodoP.Text = System.Convert.ToString(i); 
			tvMontoAmortP.Text = System.Convert.ToString(Math.Round((MontoDeuda + amortizacion),2)); 
			tvAmortizacionP.Text = System.Convert.ToString(Math.Round(amortizacion,2)); 
			tvComisionP.Text = System.Convert.ToString(Math.Round(comision,2)); 
			tvMontoDeudaP.Text = System.Convert.ToString(Math.Round(MontoDeuda,2)); 
			tvTotalAPagarP.Text = System.Convert.ToString(Math.Round(totalAPagar,2)); 
			tbrowPenultima.AddView(tvPeriodoP);
			tbrowPenultima.AddView(tvMontoAmortP);
			tbrowPenultima.AddView(tvAmortizacionP);
			tbrowPenultima.AddView(tvComisionP);
			tbrowPenultima.AddView(tvMontoDeudaP);
			tbrowPenultima.AddView(tvTotalAPagarP);
			ll.AddView(tbrowPenultima);


			TableRow tbrowUltima = new TableRow(this);
			TextView tvPeriodoF = new TextView(this);
			TextView tvMontoAmortF = new TextView(this);
			TextView tvAmortizacionF = new TextView(this);
			TextView tvComisionF = new TextView(this);
			TextView tvMontoDeudaF = new TextView(this);
			TextView tvTotalAPagarF = new TextView(this);
			tvPeriodoF.Text = "Totales";
			tvMontoAmortF.Text = System.Convert.ToString(Math.Round(sumaMontoRemanente,2)); 
			tvAmortizacionF.Text = System.Convert.ToString(Math.Round(sumaAmortizacion,2)); 
			tvComisionF.Text = System.Convert.ToString(Math.Round(sumaComision,2)); 
			tvMontoDeudaF.Text = System.Convert.ToString(Math.Round(sumaSaldo,2)); 
			tvTotalAPagarF.Text = System.Convert.ToString(Math.Round(sumaTotal * i,2));

			tbrowUltima.AddView(tvPeriodoF);
			tbrowUltima.AddView(tvMontoAmortF);
			tbrowUltima.AddView(tvAmortizacionF);
			tbrowUltima.AddView(tvComisionF);
			tbrowUltima.AddView(tvMontoDeudaF);
			tbrowUltima.AddView(tvTotalAPagarF);
			ll.AddView(tbrowUltima);

			hsv.AddView (ll);
			sv.AddView(hsv);
			builder = new AlertDialog.Builder (this);
			builder.SetTitle (Resource.String.app_name);
			builder.SetView(sv);
			builder.Create ();
			builder.Show ();
		}

		public void cuotasCrecientes() {
			ScrollView sv = new ScrollView(this);
			TableLayout ll=new TableLayout(this);
			HorizontalScrollView hsv = new HorizontalScrollView(this);
			TableRow tbrowHeader = new TableRow(this);
			TextView labelPeriodo = new TextView (this);
			TextView labelDeuda = new TextView (this);
			TextView labelAmortizacion = new TextView (this);
			TextView labelComision = new TextView (this);
			TextView labelSaldo = new TextView (this);
			TextView labelTotal = new TextView (this);
			labelPeriodo.Text = "Periodo - ";
			labelDeuda.Text = "Deuda remanente - ";
			labelAmortizacion.Text = "Amortizacion - ";
			labelComision.Text = "Comision - ";
			labelSaldo.Text = "Saldo - ";
			labelTotal.Text = "Total a pagar";
			tbrowHeader.AddView (labelPeriodo);
			tbrowHeader.AddView (labelDeuda);
			tbrowHeader.AddView (labelAmortizacion);
			tbrowHeader.AddView (labelComision);
			tbrowHeader.AddView (labelSaldo);
			tbrowHeader.AddView (labelTotal);
			ll.AddView(tbrowHeader);

			MontoDeuda = System.Convert.ToSingle (txtMontoDeuda.Text);
			PagoFinal = System.Convert.ToSingle (txtPagoFinal.Text);
			PlazoDeuda = System.Convert.ToInt16 (txtPlazoDeuda.Text);
			InteresFijo = System.Convert.ToSingle (txtInteresFijo.Text) / 100F ;
			float sumaMontoRemanente=0F,sumaComision=0F,sumaSaldo=0F,sumaTotal=0F, sumaAmortizacion = 0F;
			var amortizacion = 0F;
			var comision = 0F;
			var totalAPagar = 0F;
			var ctamor = 0F;
			for (var j = 1;j <= PlazoDeuda; j++){
				ctamor = ctamor + j;
			}
			var pagoAlFinal = MontoDeuda;
			var proAmortizacion = 0F;
			TextView tvPeriodo = new TextView(this);
			TextView tvMontoAmort = new TextView(this); 	
			TextView tvAmortizacion = new TextView (this);
			TextView tvComision = new TextView (this);
			TextView tvMontoDeuda = new TextView (this);
			TextView tvTotalAPagar = new TextView (this);
			int i = 0;
			for(i=1;i<PlazoDeuda;i++) {
				TableRow tbrow = new TableRow(this);
				proAmortizacion = (i/ctamor);
				amortizacion = (pagoAlFinal - PagoFinal) * proAmortizacion;
				comision = MontoDeuda * InteresFijo;
				MontoDeuda = MontoDeuda - amortizacion;
				totalAPagar = amortizacion + comision;
				sumaMontoRemanente = sumaMontoRemanente + MontoDeuda + amortizacion;
				sumaAmortizacion = sumaAmortizacion + amortizacion;
				sumaComision = sumaComision + comision;
				sumaSaldo = sumaSaldo + MontoDeuda;
				sumaTotal = sumaTotal + totalAPagar;
				tvPeriodo.Text = System.Convert.ToString(i); 
				tvMontoAmort.Text = System.Convert.ToString(Math.Round(MontoDeuda + amortizacion,2)); 
				tvAmortizacion.Text = System.Convert.ToString(Math.Round(amortizacion,2)); 
				tvComision.Text = System.Convert.ToString(Math.Round(comision,2)); 
				tvMontoDeuda.Text = System.Convert.ToString(Math.Round(MontoDeuda,2)); 
				tvTotalAPagar.Text = System.Convert.ToString(Math.Round(totalAPagar,2)); 
				tbrow.AddView(tvPeriodo);
				tbrow.AddView(tvMontoAmort);
				tbrow.AddView(tvAmortizacion);
				tbrow.AddView(tvComision);
				tbrow.AddView(tvMontoDeuda);
				tbrow.AddView(tvTotalAPagar);
				ll.AddView(tbrow);
			}

			TableRow tbrowPenultima = new TableRow(this);
			TextView tvPeriodoP = new TextView(this);
			TextView tvMontoAmortP = new TextView(this);
			TextView tvAmortizacionP = new TextView(this);
			TextView tvComisionP = new TextView(this);
			TextView tvMontoDeudaP = new TextView(this);
			TextView tvTotalAPagarP = new TextView(this);

			proAmortizacion = (i/ctamor);
			amortizacion = (pagoAlFinal - PagoFinal) * proAmortizacion;
			comision = MontoDeuda * InteresFijo;
			MontoDeuda = MontoDeuda - amortizacion;
			totalAPagar = amortizacion + comision +  PagoFinal;
			sumaMontoRemanente = sumaMontoRemanente + MontoDeuda + amortizacion;
			sumaAmortizacion = sumaAmortizacion + amortizacion;
			sumaComision = sumaComision + comision;
			sumaSaldo = sumaSaldo + MontoDeuda;
			sumaTotal = sumaTotal + totalAPagar;

			tvPeriodoP.Text = System.Convert.ToString(i); 
			tvMontoAmortP.Text = System.Convert.ToString(Math.Round((MontoDeuda + amortizacion),2)); 
			tvAmortizacionP.Text = System.Convert.ToString(Math.Round(amortizacion,2)); 
			tvComisionP.Text = System.Convert.ToString(Math.Round(comision,2)); 
			tvMontoDeudaP.Text = System.Convert.ToString(Math.Round(MontoDeuda,2)); 
			tvTotalAPagarP.Text = System.Convert.ToString(Math.Round(totalAPagar,2));  
			tbrowPenultima.AddView(tvPeriodoP);
			tbrowPenultima.AddView(tvMontoAmortP);
			tbrowPenultima.AddView(tvAmortizacionP);
			tbrowPenultima.AddView(tvComisionP);
			tbrowPenultima.AddView(tvMontoDeudaP);
			tbrowPenultima.AddView(tvTotalAPagarP);
			ll.AddView(tbrowPenultima);


			TableRow tbrowUltima = new TableRow(this);
			TextView tvPeriodoF = new TextView(this);
			TextView tvMontoAmortF = new TextView(this);
			TextView tvAmortizacionF = new TextView(this);
			TextView tvComisionF = new TextView(this);
			TextView tvMontoDeudaF = new TextView(this);
			TextView tvTotalAPagarF = new TextView(this);
			tvPeriodoF.Text = "Totales";
			tvMontoAmortF.Text = System.Convert.ToString(Math.Round(sumaMontoRemanente,2)); 
			tvAmortizacionF.Text = System.Convert.ToString(Math.Round(sumaAmortizacion,2)); 
			tvComisionF.Text = System.Convert.ToString(Math.Round(sumaComision,2)); 
			tvMontoDeudaF.Text = System.Convert.ToString(Math.Round(sumaSaldo,2)); 
			tvTotalAPagarF.Text = System.Convert.ToString(Math.Round(sumaTotal,2));

			tbrowUltima.AddView(tvPeriodoF);
			tbrowUltima.AddView(tvMontoAmortF);
			tbrowUltima.AddView(tvAmortizacionF);
			tbrowUltima.AddView(tvComisionF);
			tbrowUltima.AddView(tvMontoDeudaF);
			tbrowUltima.AddView(tvTotalAPagarF);
			ll.AddView(tbrowUltima);

			hsv.AddView (ll);
			sv.AddView(hsv);
			builder = new AlertDialog.Builder (this);
			builder.SetTitle (Resource.String.app_name);
			builder.SetView(sv);
			builder.Create ();
			builder.Show ();
		}

		public void cuotasDirectas() {
			ScrollView sv = new ScrollView(this);
			TableLayout ll=new TableLayout(this);
			HorizontalScrollView hsv = new HorizontalScrollView(this);
			TableRow tbrowHeader = new TableRow(this);
			TextView labelPeriodo = new TextView (this);
			TextView labelDeuda = new TextView (this);
			TextView labelAmortizacion = new TextView (this);
			TextView labelComision = new TextView (this);
			TextView labelSaldo = new TextView (this);
			TextView labelTotal = new TextView (this);
			labelPeriodo.Text = "Periodo - ";
			labelDeuda.Text = "Deuda remanente - ";
			labelAmortizacion.Text = "Amortizacion - ";
			labelComision.Text = "Comision - ";
			labelSaldo.Text = "Saldo - ";
			labelTotal.Text = "Total a pagar";
			tbrowHeader.AddView (labelPeriodo);
			tbrowHeader.AddView (labelDeuda);
			tbrowHeader.AddView (labelAmortizacion);
			tbrowHeader.AddView (labelComision);
			tbrowHeader.AddView (labelSaldo);
			tbrowHeader.AddView (labelTotal);
			ll.AddView(tbrowHeader);

			float sumaMontoRemanente=0F,sumaSaldo=0F,sumaTotal=0F;
			MontoDeuda = System.Convert.ToSingle (txtMontoDeuda.Text);
			PagoFinal = System.Convert.ToSingle (txtPagoFinal.Text);
			PlazoDeuda = System.Convert.ToInt16 (txtPlazoDeuda.Text);
			InteresFijo = System.Convert.ToSingle (txtInteresFijo.Text) / 100F ;
			var amortizacion = (MontoDeuda - PagoFinal) / PlazoDeuda;
			var comision = MontoDeuda * InteresFijo;
			var totalAPagar = 0F;
			TextView tvPeriodo = new TextView(this);
			TextView tvMontoAmort = new TextView(this); 	
			TextView tvAmortizacion = new TextView (this);
			TextView tvComision = new TextView (this);
			TextView tvMontoDeuda = new TextView (this);
			TextView tvTotalAPagar = new TextView (this);
			int i = 0;
			for(i=1;i<PlazoDeuda;i++) {
				TableRow tbrow = new TableRow(this);
				sumaMontoRemanente = sumaMontoRemanente + MontoDeuda;
				MontoDeuda = MontoDeuda - amortizacion;
				totalAPagar = amortizacion + comision;
				sumaSaldo = sumaSaldo + MontoDeuda;
				sumaTotal = sumaTotal + totalAPagar;
				tvPeriodo.Text = System.Convert.ToString(i); 
				tvMontoAmort.Text = System.Convert.ToString(Math.Round(MontoDeuda + amortizacion,2)); 
				tvAmortizacion.Text = System.Convert.ToString(Math.Round(amortizacion,2)); 
				tvComision.Text = System.Convert.ToString(Math.Round(comision,2)); 
				tvMontoDeuda.Text = System.Convert.ToString(Math.Round(MontoDeuda,2)); 
				tvTotalAPagar.Text = System.Convert.ToString(Math.Round(totalAPagar,2)); 
				tbrow.AddView(tvPeriodo);
				tbrow.AddView(tvMontoAmort);
				tbrow.AddView(tvAmortizacion);
				tbrow.AddView(tvComision);
				tbrow.AddView(tvMontoDeuda);
				tbrow.AddView(tvTotalAPagar);
				ll.AddView(tbrow);
			}

			TableRow tbrowPenultima = new TableRow(this);
			TextView tvPeriodoP = new TextView(this);
			TextView tvMontoAmortP = new TextView(this);
			TextView tvAmortizacionP = new TextView(this);
			TextView tvComisionP = new TextView(this);
			TextView tvMontoDeudaP = new TextView(this);
			TextView tvTotalAPagarP = new TextView(this);
			sumaMontoRemanente = sumaMontoRemanente + MontoDeuda;
			MontoDeuda = MontoDeuda - amortizacion - PagoFinal;
			totalAPagar = amortizacion + comision - PagoFinal;
			sumaSaldo = sumaSaldo + MontoDeuda;
			sumaTotal = sumaTotal + totalAPagar;
			tvPeriodoP.Text = System.Convert.ToString(i); 
			tvMontoAmortP.Text = System.Convert.ToString(Math.Round((MontoDeuda + amortizacion),2)); 
			tvAmortizacionP.Text = System.Convert.ToString(Math.Round(amortizacion,2)); 
			tvComisionP.Text = System.Convert.ToString(Math.Round(comision,2)); 
			tvMontoDeudaP.Text = System.Convert.ToString(Math.Round(MontoDeuda,2)); 
			tvTotalAPagarP.Text = System.Convert.ToString(Math.Round(totalAPagar,2)); 
			tbrowPenultima.AddView(tvPeriodoP);
			tbrowPenultima.AddView(tvMontoAmortP);
			tbrowPenultima.AddView(tvAmortizacionP);
			tbrowPenultima.AddView(tvComisionP);
			tbrowPenultima.AddView(tvMontoDeudaP);
			tbrowPenultima.AddView(tvTotalAPagarP);
			ll.AddView(tbrowPenultima);


			TableRow tbrowUltima = new TableRow(this);
			TextView tvPeriodoF = new TextView(this);
			TextView tvMontoAmortF = new TextView(this);
			TextView tvAmortizacionF = new TextView(this);
			TextView tvComisionF = new TextView(this);
			TextView tvMontoDeudaF = new TextView(this);
			TextView tvTotalAPagarF = new TextView(this);
			tvPeriodoF.Text = "Totales";
			tvMontoAmortF.Text = System.Convert.ToString(Math.Round(sumaMontoRemanente,2)); 
			tvAmortizacionF.Text = System.Convert.ToString(Math.Round(amortizacion,2)); 
			tvComisionF.Text = System.Convert.ToString(Math.Round(comision,2)); 
			tvMontoDeudaF.Text = System.Convert.ToString(Math.Round(sumaSaldo,2)); 
			tvTotalAPagarF.Text = System.Convert.ToString(Math.Round(sumaTotal ,2));

			tbrowUltima.AddView(tvPeriodoF);
			tbrowUltima.AddView(tvMontoAmortF);
			tbrowUltima.AddView(tvAmortizacionF);
			tbrowUltima.AddView(tvComisionF);
			tbrowUltima.AddView(tvMontoDeudaF);
			tbrowUltima.AddView(tvTotalAPagarF);
			ll.AddView(tbrowUltima);

			hsv.AddView (ll);
			sv.AddView(hsv);
			builder = new AlertDialog.Builder (this);
			builder.SetTitle (Resource.String.app_name);
			builder.SetView(sv);
			builder.Create ();
			builder.Show ();
		}

		Boolean validation(){
			bool valido = false;
			if (txtMontoDeuda.Text == "" || txtPlazoDeuda.Text == "" || txtInteresFijo.Text == "" || txtPagoFinal.Text == "" || txtMontoDeuda.Text == "." || txtPlazoDeuda.Text == "." || txtInteresFijo.Text == "." || txtPagoFinal.Text == ".") {
				valido = false;
			} else {
				valido = true;
			}
			return valido;
		}

		}

}
