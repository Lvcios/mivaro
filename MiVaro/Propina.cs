using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace MiVaro
{
	[Activity (Label = "Propina", Theme = "@style/Theme.MainWindow")]			
	public class Propina : Activity
	{
		private float floatConsumo;
		private float floatConsumoPorPersona;
		private float personas;
		private float propina;
		private EditText consumoTotal;
		private EditText consumoDividido;
		private EditText propinaPersonal;
		private Button calculaPropina;
		private RadioButton radioButtonExcelente;
		private RadioButton radioButtonBueno;
		private RadioButton radioButtonRegular;
		private RadioButton RadioButtonMalo;
		private CheckBox CheckDivide;
		private CheckBox CheckPropinaEspecifica;
		private int servicio = 1;
		private bool divide = false;
		private bool propinaEspecifica = false;
		private Android.App.AlertDialog.Builder builder;
		private string msj;
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			SetContentView (Resource.Layout.Propinas);		
			radioButtonExcelente = FindViewById<RadioButton> (Resource.Id.radioExcelente);
			radioButtonBueno = FindViewById<RadioButton> (Resource.Id.radioBueno);
			radioButtonRegular = FindViewById<RadioButton> (Resource.Id.radioRegular);
			RadioButtonMalo = FindViewById<RadioButton> (Resource.Id.radioMalo);
			CheckDivide = FindViewById<CheckBox> (Resource.Id.checkDivide);
			CheckPropinaEspecifica = FindViewById<CheckBox> (Resource.Id.checkPropina);
			consumoTotal = FindViewById<EditText>(Resource.Id.txtConsumo);
			consumoDividido = FindViewById<EditText> (Resource.Id.txtDivide);
			propinaPersonal = FindViewById<EditText> (Resource.Id.txtEspecifico);
			calculaPropina = FindViewById<Button> (Resource.Id.buttonCalculaPropina);
			radioButtonExcelente.Click += delegate {
				servicio = 1;
			};
			radioButtonBueno.Click += delegate {
				servicio = 2;
			};
			radioButtonRegular.Click += delegate {
				servicio = 3;
			};
			RadioButtonMalo.Click += delegate {
				servicio = 4;
			};

			CheckDivide.Click += delegate {
				if (divide == false){divide = true;}
				else{divide = false;}
			};

			CheckPropinaEspecifica.Click += delegate {
				if (propinaEspecifica == false){propinaEspecifica = true;}
				else{propinaEspecifica = false;}
			};
			calculaPropina.Click += delegate {
				builder = new AlertDialog.Builder(this);
				AlertDialog alert = builder.Create();
				if (consumoTotal.Text == ""){
					msj = "Te falta ingresar el consumo";
				}
				else{
				floatConsumo = System.Convert.ToSingle(consumoTotal.Text);
				if (propinaEspecifica == false){
					if (servicio == 1 && divide == false){
						propina = floatConsumo * 0.15F;
						floatConsumo = floatConsumo + propina;
							msj = string.Format("Propina: {0} \n Consumo total: {1}",Math.Round(propina,2),Math.Round(floatConsumo,2));
					}
					if (servicio == 1 && divide == true){
							if (consumoDividido.Text == ""){
								msj = "Te falta ingresar el numero de personas";
							}
							else {							
								personas = System.Convert.ToSingle(consumoDividido.Text);
								floatConsumoPorPersona = floatConsumo / personas;
								propina = floatConsumoPorPersona * 0.15F;
								floatConsumoPorPersona = floatConsumoPorPersona + propina;
								msj = string.Format("Propina por persona: {0} \nConsumo por persona: {1} \nConsumo total: {2}", Math.Round(propina,2), Math.Round(floatConsumoPorPersona,2), Math.Round((floatConsumo + propina * personas),2));
							}
					}
					if (servicio == 2 && divide == false){
						propina = floatConsumo * 0.10F;
						floatConsumo = floatConsumo + propina;
							msj = string.Format("Propina: {0} \n Consumo total: {1}",Math.Round(propina,2),Math.Round(floatConsumo,2));
					}
					if (servicio == 2 && divide == true){
							if (consumoDividido.Text == ""){
								msj = "Te falta ingresar el numero de personas";
							}
							else{
								personas = System.Convert.ToSingle(consumoDividido.Text);
								floatConsumoPorPersona = floatConsumo / personas;
								propina = floatConsumoPorPersona * 0.10F;
								floatConsumoPorPersona = floatConsumoPorPersona + propina;
								msj = string.Format("Propina por persona: {0} \nConsumo por persona: {1} \nConsumo total: {2}", Math.Round(propina,2), Math.Round(floatConsumoPorPersona,2), Math.Round((floatConsumo + propina * personas),2));
							}
					}
					if (servicio == 3 && divide == false){
						propina = floatConsumo * 0.05F;
						floatConsumo = floatConsumo + propina;
							msj = string.Format("Propina: {0} \n Consumo total: {1}",Math.Round(propina,2),Math.Round(floatConsumo,2));
					}
					if (servicio == 3 && divide == true){
							if (consumoDividido.Text == ""){
								msj = "Te falta ingresar el numero de personas";
							}
							else{	
								personas = System.Convert.ToSingle(consumoDividido.Text);
								floatConsumoPorPersona = floatConsumo / personas;
								propina = floatConsumoPorPersona * 0.05F;
								floatConsumoPorPersona = floatConsumoPorPersona + propina;
								msj = string.Format("Propina por persona: {0} \nConsumo por persona: {1} \nConsumo total: {2}", Math.Round(propina,2), Math.Round(floatConsumoPorPersona,2), Math.Round((floatConsumo + propina * personas),2));
							}
					}
					if (servicio == 4 && divide == false){
						propina = floatConsumo * 0.0F;
						floatConsumo = floatConsumo + propina;
							msj = string.Format("Propina: {0} \n Consumo total: {1}",Math.Round(propina,2),Math.Round(floatConsumo,2));
					}
					if (servicio == 4 && divide == true){
							if (consumoDividido.Text == ""){
								msj = "Te falta ingresar el numero de personas";
							}
							else {
								personas = System.Convert.ToSingle(consumoDividido.Text);
								floatConsumoPorPersona = floatConsumo / personas;
								propina = floatConsumoPorPersona * 0.0F;
								floatConsumoPorPersona = floatConsumoPorPersona + propina;
								msj = string.Format("Propina por persona: {0} \nConsumo por persona: {1} \nConsumo total: {2}", Math.Round(propina,2), Math.Round(floatConsumoPorPersona,2), Math.Round((floatConsumo + propina * personas),2));
							}
					}
				}
				else{
						if (propinaPersonal.Text == ""){
							msj = "Te falta ingresar la propina";
						}
						else
							propina = System.Convert.ToSingle(propinaPersonal.Text);
						msj = string.Format("Propina: {0} \n Consumo total: {1}",Math.Round(propina,2),Math.Round((floatConsumo + propina),2));
						}
				}
				alert.SetTitle (Resource.String.app_name);
				alert.SetMessage (msj);
				alert.Show();
			};
		}
	}
}

