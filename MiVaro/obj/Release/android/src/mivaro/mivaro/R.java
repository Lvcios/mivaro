/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * aapt tool from the resource data it found.  It
 * should not be modified by hand.
 */

package mivaro.mivaro;

public final class R {
    public static final class array {
        public static final int afore36=0x7f040000;
        public static final int afore45=0x7f040001;
        public static final int afore59=0x7f040002;
        public static final int afore60=0x7f040003;
    }
    public static final class attr {
        /** <p>Must be a string value, using '\\;' to escape characters such as '\\n' or '\\uxxxx' for a unicode character.
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
         */
        public static final int customFont=0x7f010000;
    }
    public static final class drawable {
        public static final int acercade=0x7f020000;
        public static final int afore_hasta_36=0x7f020001;
        public static final int afore_hasta_45=0x7f020002;
        public static final int afore_hasta_59=0x7f020003;
        public static final int afore_masde_sesenta=0x7f020004;
        public static final int boton_afores=0x7f020005;
        public static final int boton_autofinanciamiento=0x7f020006;
        public static final int boton_home=0x7f020007;
        public static final int boton_miquincena=0x7f020008;
        public static final int boton_no_propinas=0x7f020009;
        public static final int boton_prestamoconintereses=0x7f02000a;
        public static final int boton_propinas=0x7f02000b;
        public static final int boton_si_propinas=0x7f02000c;
        public static final int boton_tabla=0x7f02000d;
        public static final int boton_tablasamorti=0x7f02000e;
        public static final int fonts=0x7f02000f;
        public static final int icon=0x7f020010;
        public static final int icono_comida=0x7f020011;
        public static final int icono_emergencia=0x7f020012;
        public static final int icono_entretenimiento=0x7f020013;
        public static final int icono_servicio_bueno=0x7f020014;
        public static final int icono_servicio_excelente=0x7f020015;
        public static final int icono_servicio_malo=0x7f020016;
        public static final int icono_servicio_regular=0x7f020017;
        public static final int icono_transporte=0x7f020018;
        public static final int logo120x120=0x7f020019;
        public static final int logo512_512=0x7f02001a;
        public static final int logo60_60=0x7f02001b;
        public static final int mivaro=0x7f02001c;
        public static final int splash_2=0x7f02001d;
        public static final int splash_background=0x7f02001e;
    }
    public static final class id {
        public static final int ButtonAfore=0x7f070040;
        public static final int ButtonAmort=0x7f070042;
        public static final int ButtonAuto=0x7f070043;
        public static final int ButtonMiQuincena=0x7f07003e;
        public static final int ButtonPrestamos=0x7f070041;
        public static final int ButtonPropinas=0x7f07003f;
        public static final int btnAcercade=0x7f07003d;
        public static final int btnGuardar=0x7f070051;
        public static final int btnHistorial=0x7f070052;
        public static final int buttonCalculaAmort=0x7f070032;
        public static final int buttonCalculaPropina=0x7f070061;
        public static final int buttonCalcular=0x7f070056;
        public static final int buttonCalcularAfore=0x7f070027;
        public static final int buttonCalulaAutoFin=0x7f070038;
        public static final int buttonEligeAfore=0x7f07001d;
        public static final int capitalInicial=0x7f070053;
        public static final int checkComida=0x7f07004b;
        public static final int checkDivide=0x7f07005e;
        public static final int checkEmergencias=0x7f07004d;
        public static final int checkEntretenimiento=0x7f070049;
        public static final int checkIngreso=0x7f070045;
        public static final int checkPropina=0x7f070060;
        public static final int checkTransporte=0x7f070050;
        public static final int imageView1=0x7f07003c;
        public static final int imageView2=0x7f070003;
        public static final int imageView3=0x7f070007;
        public static final int linearLayout1=0x7f070000;
        public static final int linearLayout10=0x7f07000b;
        public static final int linearLayout11=0x7f07000c;
        public static final int linearLayout12=0x7f07000d;
        public static final int linearLayout13=0x7f070013;
        public static final int linearLayout14=0x7f070016;
        public static final int linearLayout15=0x7f070019;
        public static final int linearLayout16=0x7f07001c;
        public static final int linearLayout17=0x7f070022;
        public static final int linearLayout18=0x7f07001f;
        public static final int linearLayout19=0x7f070026;
        public static final int linearLayout2=0x7f070039;
        public static final int linearLayout4=0x7f070008;
        public static final int linearLayout5=0x7f070028;
        public static final int linearLayout6=0x7f070001;
        public static final int linearLayout7=0x7f070002;
        public static final int linearLayout8=0x7f070004;
        public static final int linearLayout9=0x7f070006;
        public static final int meses=0x7f070055;
        public static final int myButton=0x7f07000a;
        public static final int radioAfore36=0x7f07000f;
        public static final int radioAfore45=0x7f070010;
        public static final int radioAfore59=0x7f070011;
        public static final int radioAfore60=0x7f070012;
        public static final int radioBueno=0x7f07005a;
        public static final int radioConstante=0x7f07002e;
        public static final int radioCreciente=0x7f070030;
        public static final int radioDecreciente=0x7f07002f;
        public static final int radioExcelente=0x7f070059;
        public static final int radioFlat=0x7f070031;
        public static final int radioGroup1=0x7f07000e;
        public static final int radioGroupPropina=0x7f070058;
        public static final int radioMalo=0x7f07005c;
        public static final int radioRegular=0x7f07005b;
        public static final int tableLayout1=0x7f07003a;
        public static final int tableRow1=0x7f07003b;
        public static final int tasa=0x7f070054;
        public static final int textView11=0x7f070023;
        public static final int textView12=0x7f070020;
        public static final int textView2=0x7f070047;
        public static final int textView3=0x7f070046;
        public static final int textView4=0x7f070029;
        public static final int textView5=0x7f070005;
        public static final int textView6=0x7f070009;
        public static final int textView7=0x7f070014;
        public static final int textView8=0x7f070017;
        public static final int textView9=0x7f07001a;
        public static final int txtAfore=0x7f07001e;
        public static final int txtAnioPago=0x7f070025;
        public static final int txtAniosTrabajo=0x7f070024;
        public static final int txtComida=0x7f07004a;
        public static final int txtConsumo=0x7f070057;
        public static final int txtDivide=0x7f07005d;
        public static final int txtEmergencias=0x7f07004c;
        public static final int txtEnganche=0x7f070034;
        public static final int txtEntretenimiento=0x7f070048;
        public static final int txtEspecifico=0x7f07005f;
        public static final int txtIngreso=0x7f070044;
        public static final int txtInteres=0x7f070037;
        public static final int txtInteresFijo=0x7f07002c;
        public static final int txtMonto=0x7f070035;
        public static final int txtMontoDeuda=0x7f07002a;
        public static final int txtMontoInicial=0x7f070021;
        public static final int txtPagoFinal=0x7f07002d;
        public static final int txtPlazo=0x7f070036;
        public static final int txtPlazoDeuda=0x7f07002b;
        public static final int txtPorcentajeDeposito=0x7f07001b;
        public static final int txtPrecioAuto=0x7f070033;
        public static final int txtSueldoMensual=0x7f070015;
        public static final int txtTasaIncremento=0x7f070018;
        public static final int txtTransporte=0x7f07004f;
        public static final int txtViewTrans=0x7f07004e;
    }
    public static final class layout {
        public static final int acercade=0x7f030000;
        public static final int afore=0x7f030001;
        public static final int amortizacion=0x7f030002;
        public static final int autofin=0x7f030003;
        public static final int main=0x7f030004;
        public static final int miquincena=0x7f030005;
        public static final int prestamos=0x7f030006;
        public static final int propinas=0x7f030007;
    }
    public static final class string {
        public static final int app_name=0x7f050001;
        public static final int hello=0x7f050000;
        public static final int listAforeNot=0x7f050004;
        public static final int listAforeOk=0x7f050003;
        public static final int noDBSuport=0x7f050006;
        public static final int totalPropina=0x7f050002;
        public static final int validacionNot=0x7f050005;
    }
    public static final class style {
        public static final int Theme_MainWindow=0x7f060001;
        public static final int Theme_Splash=0x7f060000;
    }
    public static final class styleable {
        /** Attributes that can be used with a CustomFonts.
           <p>Includes the following attributes:</p>
           <table>
           <colgroup align="left" />
           <colgroup align="left" />
           <tr><th>Attribute</th><th>Description</th></tr>
           <tr><td><code>{@link #CustomFonts_customFont MiVaro.MiVaro:customFont}</code></td><td></td></tr>
           </table>
           @see #CustomFonts_customFont
         */
        public static final int[] CustomFonts = {
            0x7f010000
        };
        /**
          <p>This symbol is the offset where the {@link MiVaro.MiVaro.R.attr#customFont}
          attribute's value can be found in the {@link #CustomFonts} array.


          <p>Must be a string value, using '\\;' to escape characters such as '\\n' or '\\uxxxx' for a unicode character.
<p>This may also be a reference to a resource (in the form
"<code>@[<i>package</i>:]<i>type</i>:<i>name</i></code>") or
theme attribute (in the form
"<code>?[<i>package</i>:][<i>type</i>:]<i>name</i></code>")
containing a value of this type.
          @attr name android:customFont
        */
        public static final int CustomFonts_customFont = 0;
    };
}
